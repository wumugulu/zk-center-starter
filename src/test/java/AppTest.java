import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.CharsetUtil;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.DefaultPropertySourceFactory;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.ResourcePropertySource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class AppTest {

    public static void main(String[] args) throws IOException {
//        new ResourcePropertySource()
//        YamlPropertiesFactoryBean factory = new YamlPropertiesFactoryBean();
//        new EncodedResource();
//        DefaultPropertySourceFactory
        String filename = "D:\\software\\dev\\apache-tomcat-8.5.49\\webapps\\ROOT\\WEB-INF\\classes\\application-dev.yml";
        String nodeName = "abc.yml";
        String nodeValue = FileUtil.readString(filename, CharsetUtil.UTF_8);
        ByteArrayResource byteArrayResource = new ByteArrayResource(nodeValue.getBytes(StandardCharsets.UTF_8));
        List<PropertySource<?>> sources = new YamlPropertySourceLoader().load(nodeName, byteArrayResource);
        for (PropertySource<?> source : sources) {
            System.err.println(source.getName() + source.toString());
        }
    }

}
