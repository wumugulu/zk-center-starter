-- DROP TABLE IF EXISTS `t_staff`;
CREATE TABLE IF NOT EXISTS `t_staff`
(
    `id`   int(10) not null primary key,
    `name` varchar(80) not null,
    `age`  int(4),
    `memo` varchar(200),
    `update_time`  datetime
);
