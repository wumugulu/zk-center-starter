function netRequest(actionObj) {
    var url;
    switch (actionObj.action) {
        case "home":
            url = '/zkcenter/api/home';
            break;
        case "listProject":
            url = '/zkcenter/api/listProject';
            break;
        case "listApplication":
            url = '/zkcenter/api/listApplication';
            break;
        case "listProfile":
            url = '/zkcenter/api/listProfile';
            break;
        case "listConfig":
            url = '/zkcenter/api/listConfig';
            break;
        case "add":
            url = '/zkcenter/api/add';
            break;
        case "update":
            url = '/zkcenter/api/update';
            break;
        case "remove":
            url = '/zkcenter/api/remove';
            break;
        default:
            break;
    }

    const obj = {
        method: 'POST',
        url: url,
        data: actionObj.param
    }

    return new Promise((resolve, reject) => {
        axios(obj)
            .then(ret=> {
                return resolve(ret)
            })
            .catch(err=>{
                return reject(err)
            });
    })
}