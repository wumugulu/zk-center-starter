package com.wumugulu.zkcenter.ui.controller;

import com.wumugulu.zkcenter.config.annotation.ZkCenterScope;
import com.wumugulu.zkcenter.config.annotation.ZkCenterValue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@ZkCenterScope
@RestController
@RequestMapping("/zkcenter/demo")
public class DemoController {

    @Value("${ab.cd.ef:}")
    private String ymlValue;
    @GetMapping("/yml")
    public String demoYml() {
        return ymlValue;
    }

    @Value("${redis.server:}")
    private String propValue;
    @GetMapping("/prop")
    public String demoProp() {
        return propValue;
    }

    @ZkCenterValue("test.xml")
    private String xmlValue = null;
    @GetMapping("/xml")
    public String demoXml() {
        return xmlValue;
    }

}
