package com.wumugulu.zkcenter.ui.controller;

import cn.hutool.db.Entity;
import com.wumugulu.zkcenter.config.bean.ZkCenterSetting;
import com.wumugulu.zkcenter.ui.entity.NodeEntity;
import com.wumugulu.zkcenter.ui.entity.StaffEntity;
import com.wumugulu.zkcenter.ui.service.ZkCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/zkcenter/api")
public class ZkCenterController {

    @Autowired
    private ZkCenterSetting zkCenterSetting;

    @Autowired
    private ZkCenterService zkcenterService;

    @RequestMapping("/setting")
    public Object setting() throws IOException {
        return zkCenterSetting;
    }


    @RequestMapping("/listProject")
    public List<String> listProject() {
        return zkcenterService.listProject();
    }

    @RequestMapping("/listApplication")
    public List<String> listApplication(@RequestBody NodeEntity node) {
        String projectName = node.getProjectName();
        return zkcenterService.listApplication(projectName);
    }

    @RequestMapping("/listProfile")
    public List<String> listProfile(@RequestBody NodeEntity node) {
        String projectName = node.getProjectName();
        String applicationName = node.getApplicationName();
        return zkcenterService.listProfile(projectName, applicationName);
    }




    @RequestMapping("/listConfig")
    public List<NodeEntity> listConfig(@RequestBody NodeEntity node) {
        return zkcenterService.listConfig(node);
    }

    @RequestMapping("/list2")
    public List<Entity> list2() throws IOException {
        return zkcenterService.list2();
    }

    @RequestMapping("/add")
    public Integer add(@RequestBody NodeEntity node) {
        System.err.println("zkCenterController.add: " + node.toString());
        return zkcenterService.add(node);
    }

    @RequestMapping("/add2")
    public Integer add2(@RequestBody StaffEntity bean) {
        StaffEntity staff = new StaffEntity();
        staff.setId(bean.getId());
        staff.setName(bean.getName());
        staff.setAge(bean.getAge());
        staff.setMemo(bean.getMemo());
        staff.setUpdateTime(new Date());
        System.err.println("controller add: " + staff);

        return zkcenterService.add2(staff);
    }

    @RequestMapping("/update")
    public Integer update(@RequestBody NodeEntity node) {
        System.err.println("zkCenterController.update: " + node.toString());
        return zkcenterService.update(node);
    }

    @RequestMapping("/update2")
    public Integer update2(@RequestBody StaffEntity staff) {
        return zkcenterService.update2(staff);
    }

    @RequestMapping("/remove")
    public Integer remove(@RequestBody NodeEntity node) {
        System.err.println("zkCenterController.remove: " + node.toString());
        return zkcenterService.remove(node);
    }

    @RequestMapping("/remove2")
    public Integer remove2(@RequestBody StaffEntity staff) {
        System.err.println("controller remove: " + staff);
        return zkcenterService.remove2(staff);
    }

}
