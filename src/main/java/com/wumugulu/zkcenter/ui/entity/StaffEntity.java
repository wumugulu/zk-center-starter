package com.wumugulu.zkcenter.ui.entity;

import lombok.Data;

import java.util.Date;

@Data
public class StaffEntity {
    private Integer id;
    private String name;
    private Integer age;
    private String memo;
    private Date updateTime;
}
