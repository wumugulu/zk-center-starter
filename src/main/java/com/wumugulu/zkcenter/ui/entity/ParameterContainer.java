package com.wumugulu.zkcenter.ui.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ParameterContainer {
    //节点相对路径
    private String nodePath;
    //节点层级(暂时无用)
    private Integer nodeLevel;
    //引用节点值的bean对象
    private Object beanObject;
    //bean中有相应注解的属性field
    private String beanField;
}
