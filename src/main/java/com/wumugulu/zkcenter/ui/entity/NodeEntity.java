package com.wumugulu.zkcenter.ui.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class NodeEntity {
    private String projectName;
    private String applicationName;
    private String activeProfile;
    private String configClass;
    private String configName;
    private String configValue;

    private Long czxid; // Creation ID
    private Long mzxid; // Modified ID
    private Long ctime; // Creation Time
    private Long mtime; // Last Modified Time
    private Integer version; // Data Version
    private Integer cversion; // Children Version
    private Integer aversion; // ACL Version
    private Long ephemeralOwner; // Ephemeral Owner
    private Integer dataLength; // Data Length
    private Integer numChildren; // Number Of Children
    private Long pzxid; // Node ID
}
