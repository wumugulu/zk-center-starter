package com.wumugulu.zkcenter.ui.service;

import cn.hutool.core.io.FileUtil;
import cn.hutool.db.Db;
import cn.hutool.db.DbUtil;
import cn.hutool.db.Entity;
import cn.hutool.db.ds.simple.SimpleDataSource;
import com.wumugulu.zkcenter.ui.dao.ZkCenterDao;
import com.wumugulu.zkcenter.ui.entity.NodeEntity;
import com.wumugulu.zkcenter.ui.entity.StaffEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.io.File;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@Service
public class ZkCenterService {

    @Autowired
    private ZkCenterDao zkCenterDao;

    // private String driver = "org.h2.Driver";
    private String url = "jdbc:h2:file:{TMP_PATH}db_h2";// "jdbc:h2:mem:testdb;DB_CLOSE_ON_EXIT=TRUE";
    private String username = "sa";
    private String password = "123456";
    private Db dbZkCenter = null;

    public ZkCenterService() {
        String userHomeDir = FileUtil.getUserHomePath();// System.getProperty("user.home");
        userHomeDir += userHomeDir.endsWith(File.separator)? "": File.separator;
        System.err.println("userHomeDir = " + userHomeDir);
        url = url.replace("{TMP_PATH}", userHomeDir);
        System.err.println("url = " + url);

        DataSource dataSource = new SimpleDataSource(url, username, password);
        dbZkCenter = DbUtil.use(dataSource);

        boolean needInit = false;
        String tableCheck = "t_staff"; // ""select count(1) from t_staff";
        try {
            System.err.println("checking H2 Database ......");
            dbZkCenter.count(tableCheck);
        } catch (SQLException e) {
            //e.printStackTrace();
            System.err.println("table not found! init H2 database ...");
            needInit = true;
        }

        if (needInit) {
            try {
                System.err.println("creating table ......");
                String str1 = FileUtil.readString("classpath:H2/schema.sql", "UTF-8");
                dbZkCenter.execute(str1);

                System.err.println("inserting data ......");
                String str2 = FileUtil.readString("classpath:H2/data.sql", "UTF-8");
                dbZkCenter.execute(str2);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<NodeEntity> listConfig(NodeEntity node) {
        return zkCenterDao.listConfig(node);
    }

    public List<Entity> list2() {
//        dbZkCenter.list();

        String sqlQuery = "select * from t_staff";
        try {
            if (dbZkCenter == null)
                return null;
            else
                return dbZkCenter.query(sqlQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Integer add(NodeEntity node) {
        return zkCenterDao.add(node);
    }

    public Integer add2(StaffEntity staff) {
        Entity entity = Entity.create()
                .setTableName("t_staff")
                .parseBean(staff, true, true);
        System.err.println("add ... entity = " + entity);

        try {
            return dbZkCenter.insert(entity);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public Integer update(NodeEntity node) {
        return zkCenterDao.update(node);
    }

    public Integer update2(StaffEntity staff) {
        StaffEntity staffUpdate = new StaffEntity();
        staffUpdate.setName(staff.getName());
        staffUpdate.setAge(staff.getAge());
        staffUpdate.setMemo(staff.getMemo());
        staffUpdate.setUpdateTime(new Date());
        Entity entityUpdate = Entity.create("t_staff")
                .parseBean(staffUpdate, true, true);
        System.err.println("update ... entityUpdate = " + entityUpdate);

        StaffEntity staffWhere = new StaffEntity();
        staffWhere.setId(staff.getId());
        Entity entityWhere = Entity.create()
                .parseBean(staffWhere, true, true);
        System.err.println("where ... entityWhere = " + entityWhere);

        try {
            return dbZkCenter.update(entityUpdate, entityWhere);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public Integer remove(NodeEntity node) {
        return zkCenterDao.remove(node);
    }

    public Integer remove2(StaffEntity staff) {
        System.err.println("remove ... staff = " + staff);
        try {
            return dbZkCenter.del("t_staff", "id", staff.getId());
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public List<String> listProject() {
        return zkCenterDao.listProject();
    }

    public List<String> listApplication(String projectName) {
        return zkCenterDao.listApplication(projectName);
    }

    public List<String> listProfile(String projectName, String applicationName) {
        return zkCenterDao.listProfile(projectName, applicationName);
    }
}
