package com.wumugulu.zkcenter.ui.dao;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import com.wumugulu.zkcenter.config.bean.ZkCenterSetting;
import com.wumugulu.zkcenter.ui.entity.NodeEntity;
import org.apache.curator.framework.CuratorFramework;
import org.apache.zookeeper.data.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ZkCenterDao {

    @Autowired
    private ZkCenterSetting zkCenterSetting;

    @Autowired
    private CuratorFramework curatorClient;

    private String getRootPath(String... path) {
        String fullPath = StrUtil.join("/", path);
        return fullPath;
    }

    public List<NodeEntity> listConfig(NodeEntity node) {
        String zkRootPathWatch = StrUtil.join("/", ZkCenterSetting.ZKCENTER_PATH_PREFIX, node.getProjectName(), node.getApplicationName(), node.getActiveProfile());
        try {
            List<NodeEntity> nodeEntityList = new ArrayList<>();
            for (String nodeName : curatorClient.getChildren().forPath(zkRootPathWatch)) {
                String nodePath = zkRootPathWatch + "/" + nodeName;
                Stat nodeStat = new Stat();
                String nodeValue = new String(curatorClient.getData().storingStatIn(nodeStat).forPath(nodePath), CharsetUtil.GBK);

                NodeEntity nodeEntity = new NodeEntity();
                BeanUtil.copyProperties(nodeStat, nodeEntity);

                nodeEntity.setProjectName(zkCenterSetting.getProjectName());
                nodeEntity.setApplicationName(zkCenterSetting.getApplicationName());
                nodeEntity.setActiveProfile(zkCenterSetting.getActiveProfile());
                nodeEntity.setConfigClass(FileUtil.extName(nodeName));
                nodeEntity.setConfigName(nodeName);
                nodeEntity.setConfigValue(nodeValue);

                nodeEntityList.add(nodeEntity);
            }
            return nodeEntityList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getCurrentNodePath(NodeEntity node) {
        return StrUtil.join("/", ZkCenterSetting.ZKCENTER_PATH_PREFIX, node.getProjectName(), node.getApplicationName(), node.getActiveProfile(), node.getConfigName());
    }

    public Integer add(NodeEntity node) {
        System.err.println("ZkCenterDao.add: " + node.toString());
        try {
            String result = curatorClient.create().forPath(getCurrentNodePath(node), node.getConfigValue().getBytes(CharsetUtil.GBK));
            System.err.println("after add: " + result);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public Integer update(NodeEntity node) {
        System.err.println("ZkCenterDao.update: " + node.toString());
        try {
            Stat stat = curatorClient.setData().forPath(getCurrentNodePath(node), node.getConfigValue().getBytes(CharsetUtil.GBK));
            System.err.println("after update: " + stat.toString());
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public Integer remove(NodeEntity node) {
        System.err.println("ZkCenterDao.remove: " + node.toString());
        try {
            curatorClient.delete().forPath(getCurrentNodePath(node));
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }



    public List<String> listProject() {
        String zkFetchPath = ZkCenterSetting.ZKCENTER_PATH_PREFIX;
        try {
            List<String> list = curatorClient.getChildren().forPath(zkFetchPath);
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<String> listApplication(String projectName) {
        String zkFetchPath = ZkCenterSetting.ZKCENTER_PATH_PREFIX + "/" + projectName;
        try {
            List<String> list = curatorClient.getChildren().forPath(zkFetchPath);
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<String> listProfile(String projectName, String applicationName) {
        String zkFetchPath = ZkCenterSetting.ZKCENTER_PATH_PREFIX + "/" + projectName + "/" + applicationName;
        try {
            List<String> list = curatorClient.getChildren().forPath(zkFetchPath);
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
