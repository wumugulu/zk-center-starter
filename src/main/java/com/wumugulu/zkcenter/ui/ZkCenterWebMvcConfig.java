package com.wumugulu.zkcenter.ui;

import com.wumugulu.zkcenter.config.bean.ZkCenterSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

public class ZkCenterWebMvcConfig implements WebMvcConfigurer {

    // set the path accessing html page of zkcenter via yml config file
    // after that, you can access http://xxx.yyy.com/#{viewPath}/index.html to manage the data of zkcenter
    // at the same time, another tow parameter username and password will identify the user

    @Autowired
    private ZkCenterSetting zkCenterSetting;

    /*添加zkcenter的静态页面到现有的web工程中*/
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        System.err.println("ZkCenterWebMvcConfig is init ... : " + zkCenterSetting);
        registry.addResourceHandler("/" + zkCenterSetting.getConfigViewPath() + "/**")
                .addResourceLocations("classpath:/zkcenter-ui/");
    }

//    @Override
//    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("/"+viewPath+"/").setViewName("/zkcenter-ui/index.html");
//    }
}
