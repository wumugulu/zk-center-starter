package com.wumugulu.zkcenter;

import cn.hutool.core.util.ArrayUtil;
import com.wumugulu.zkcenter.config.annotation.EnableZkCenter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

// dev ui functions ... use the url below
// http://localhost:7099/zkcenter-ui/index.html
@EnableZkCenter
@SpringBootApplication
public class ZkCenterApplication {

    public static void main(String[] args) {
        String[] myArgs = new String[8];
        /*
        zkcenter:
            address: 127.0.0.4:2181
            project: egmall
            context-path: mycenter
            username: root
            password: 123456
        myArgs[0] = "--server.port=7099";
        myArgs[1] = "--spring.profiles.active=dev";
        myArgs[2] = "--spring.application.name=provider-account-7041";
        myArgs[3] = "--zkcenter.address=127.0.0.4:2181";
        myArgs[4] = "--zkcenter.project=egmall";
        myArgs[5] = "--zkcenter.context-path=mycenter";
        myArgs[6] = "--zkcenter.username=root";
        myArgs[7] = "--zkcenter.password=123456";
         */

        List<String> argList = new ArrayList<>();
        argList.add("--server.port=7099");
        argList.add("--spring.profiles.active=dev");
        argList.add("--spring.application.name=provider-account-7041");
        argList.add("--zkcenter.address=127.0.0.4:2181");
        argList.add("--zkcenter.project=egmall");
        argList.add("--zkcenter.context-path=mycenter");
        argList.add("--zkcenter.username=root");
        argList.add("--zkcenter.password=123456");

        String[] argsArray = argList.toArray(new String[0]);

        SpringApplication.run(ZkCenterApplication.class, argsArray);
    }

}
