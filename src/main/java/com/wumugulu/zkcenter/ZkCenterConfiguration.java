package com.wumugulu.zkcenter;

import com.wumugulu.zkcenter.config.bean.ZkCenterAppListener;
import com.wumugulu.zkcenter.config.bean.ZkCenterSetting;
import com.wumugulu.zkcenter.ui.ZkCenterWebMvcConfig;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryNTimes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.File;

/**
 *
 * zookeeper配置中心设计思路:
 *
 * # 1.群组(项目)名称(eg:B2B.ddjk)
 * projectName
 *
 * # 2.运行环境(eg:dev/dev-fky/test/prod)
 * spring.profiles.active
 *
 * # 3.应用程序名(eg:ddky-branch-web/ddky-b2b-provider)
 * spring.application.name
 *
 * # 4.配置文件(eg:jdbc.yml/redis.properties/logging.xml)
 * save every properties/yaml file into a zk node
 * nodename=fileName
 * nodevalue=fileContent
 *
 */


@Configuration
@ComponentScan // add this annotation to scan controller, service, mapper etc.
public class ZkCenterConfiguration {

    @Autowired
    private ZkCenterSetting zkCenterSetting;

    @Bean
    public ZkCenterSetting getZkCenterSetting() {
        return new ZkCenterSetting();
    }

    @Bean
    public ZkCenterAppListener getZkCenterAppListener() {
        return new ZkCenterAppListener();
    }

    @Bean
    public ZkCenterWebMvcConfig getZkCenterWebMvcConfig() {
        return new ZkCenterWebMvcConfig();
    }

    @Bean
    public CuratorFramework getCuratorFramework() {
        String zkServerAddress = zkCenterSetting.getConfigAddress();
        CuratorFramework curatorClient = CuratorFrameworkFactory.newClient(zkServerAddress, new RetryNTimes(3, 1000));
        curatorClient.start();

        return curatorClient;
    }
}

