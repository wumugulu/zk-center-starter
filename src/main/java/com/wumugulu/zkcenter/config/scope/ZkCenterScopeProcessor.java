package com.wumugulu.zkcenter.config.scope;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.wumugulu.zkcenter.config.annotation.ZkCenterValue;
import com.wumugulu.zkcenter.config.bean.ZkCenterAppListener;
import com.wumugulu.zkcenter.config.bean.ZkCenterSetting;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.Scope;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

// Spring Boot 如何实现Bean的刷新
// https://juejin.cn/post/6844903816513454093
public class ZkCenterScopeProcessor implements Scope {

    //用来缓存被实例的对象
    private ConcurrentHashMap map = new ConcurrentHashMap();

    @Override
    public Object get(String beanName, ObjectFactory<?> objectFactory) {
        if (map.containsKey(beanName)) {
            return map.get(beanName);
        }
        else {
            Object object = objectFactory.getObject();
            // System.err.println("objectFactory.getObject() " + beanName);
            // 完善:
            perfectNewBean(beanName, object);
            map.put(beanName, object);
            return object;
        }
    }

    @Override
    public Object remove(String beanName) {
        return map.remove(beanName);
    }

    @Override
    public void registerDestructionCallback(String s, Runnable runnable) {}

    @Override
    public Object resolveContextualObject(String s) {
        return null;
    }

    @Override
    public String getConversationId() {
        return null;
    }

    private String readLocalConfigData(String filename) {
        ZkCenterSetting zkCenterSetting = SpringUtil.getBean(ZkCenterSetting.class);
        String userHomeDir = FileUtil.getUserHomePath();
        String localSavedPath = userHomeDir + (userHomeDir.endsWith(File.separator) ? "" : File.separator) + "localZkCenter" + File.separator + zkCenterSetting.getProjectName() + File.separator;
        if (!FileUtil.exist(localSavedPath) || !FileUtil.isDirectory(localSavedPath))
            return null;

        String fullFileName = localSavedPath + filename;
        if (FileUtil.exist(fullFileName)) {
            return FileUtil.readString(fullFileName, Charset.defaultCharset());
        } else {
            return null;
        }
    }

    private void perfectNewBean(String beanName, Object beanObj) {
        Arrays.stream(beanObj.getClass().getDeclaredFields()).forEach(field -> {
            ZkCenterValue annotation = field.getAnnotation(ZkCenterValue.class);
            if (annotation!=null){
                String annotationValue = annotation.value();
                String localFileContent = readLocalConfigData(annotationValue);
                if (!StrUtil.isEmpty( localFileContent ))
                    BeanUtil.setFieldValue(beanObj, field.getName(), localFileContent);
            }
        });
    }

}

