package com.wumugulu.zkcenter.config.annotation;

import com.wumugulu.zkcenter.ZkCenterConfiguration;
import com.wumugulu.zkcenter.ui.ZkCenterWebMvcConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// enable zookeeper config center function, include publish param's changing, controllers and html ui

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Import({ZkCenterConfiguration.class})
public @interface EnableZkCenter {
//    String address() default "127.0.0.1:2181";
//    String project() default "nullProjectName";
}
