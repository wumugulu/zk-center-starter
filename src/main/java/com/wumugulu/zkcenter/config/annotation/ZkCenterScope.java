package com.wumugulu.zkcenter.config.annotation;

import com.wumugulu.zkcenter.config.bean.ZkCenterConstant;
import org.springframework.context.annotation.Scope;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Scope(
        scopeName = ZkCenterConstant.ZKCENTER_SCOPE
)
public @interface ZkCenterScope {
}
