package com.wumugulu.zkcenter.config.bean;

import org.springframework.beans.factory.annotation.Value;

public class ZkCenterSetting {

    public static String ZKCENTER_PATH_PREFIX = "/zkcenter";
/*
    // zookeeper的连接串,如:127.0.0.1:2181
    private String watchZkAddress;
    // zookeeper中 zkCenter的根路径,如:/zkCenter/project/application/dev
    private String watchRootPath;

    // 用于管理配置数据的ui路径,如 http://localhost:8088/[viewPath]/zkCenter.html
    private String viewPath = "tmpViewPath";
    // 应用的配置数据暂存于本地的文件(完整全路径,包含最后的"/",不包含文件名)
    private String localPath = "tmpViewPath";
*/

    /**
     * from config file(yaml and properties)
     */
    @Value("${spring.application.name:application}")
    private String applicationName;
    @Value("${spring.profiles.active:xyz}")
    private String activeProfile;

    @Value("${zkcenter.address:127.0.0.1:2181}")
    private String configAddress;
    @Value("${zkcenter.context-path:zkcenter-ui}")
    private String configViewPath;
    @Value("${zkcenter.project:nullProjectName}")
    private String projectName;
    @Value("${zkcenter.username:nullUsername}")
    private String username;
    @Value("${zkcenter.password:nullPassword}")
    private String password;

//    public ZkCenterSetting() {
//        System.getProperties().forEach((key, value)-> System.err.println("... env-1: " + key + " = " + value));
//        System.err.println("applicationName=" + applicationName);
//        watchZkAddress = configAddress;
//        watchRootPath += configProject + "/" + applicationName + "/" + currentProfile;
//
//        viewPath = configViewPath;
//        String userHomeDir = FileUtil.getUserHomePath();
//        localPath += userHomeDir + (userHomeDir.endsWith(File.separator) ? "" : File.separator) + File.separator + configProject + File.separator;
//    }


/*
    public String getWatchZkAddress() {
        this.watchZkAddress = this.configAddress;
        return watchZkAddress;
    }

    public void setWatchZkAddress(String watchZkAddress) {
        this.watchZkAddress = watchZkAddress;
    }

    public String getWatchRootPath() {
        this.watchRootPath = "/zkcenter/" + this.configProject + "/" + this.applicationName + "/" + this.currentProfile;
        return watchRootPath;
    }

    public void setWatchRootPath(String watchRootPath) {
        this.watchRootPath = watchRootPath;
    }

    public String getViewPath() {
        this.viewPath = this.configViewPath;
        return viewPath;
    }

    public void setViewPath(String viewPath) {
        this.viewPath = viewPath;
    }

    public String getLocalPath() {
        String userHomeDir = FileUtil.getUserHomePath();
        this.localPath = "tmpViewPath" + userHomeDir + (userHomeDir.endsWith(File.separator) ? "" : File.separator) + File.separator + configProject + File.separator;
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }
*/
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getActiveProfile() {
        return activeProfile;
    }

    public void setActiveProfile(String activeProfile) {
        this.activeProfile = activeProfile;
    }

    public String getConfigAddress() {
        return configAddress;
    }

    public void setConfigAddress(String configAddress) {
        this.configAddress = configAddress;
    }

    public String getConfigViewPath() {
        return configViewPath;
    }

    public void setConfigViewPath(String configViewPath) {
        this.configViewPath = configViewPath;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @Override
    public String toString() {
        return "ZkCenterSetting{" +
                "applicationName='" + applicationName + '\'' +
                ", activeProfile='" + activeProfile + '\'' +
                ", configAddress='" + configAddress + '\'' +
                ", configViewPath='" + configViewPath + '\'' +
                ", projectName='" + projectName + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
