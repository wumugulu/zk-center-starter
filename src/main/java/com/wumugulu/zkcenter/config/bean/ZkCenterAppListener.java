package com.wumugulu.zkcenter.config.bean;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileNameUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.wumugulu.zkcenter.config.annotation.EnableZkCenter;
import com.wumugulu.zkcenter.config.annotation.ZkCenterScope;
import com.wumugulu.zkcenter.config.annotation.ZkCenterValue;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.TreeCache;
import org.apache.curator.retry.RetryNTimes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.ByteArrayResource;

import java.io.File;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;


@Slf4j
public class ZkCenterAppListener implements ApplicationListener<ContextRefreshedEvent>{

    @Autowired
    private ZkCenterSetting zkCenterSetting;

    @Autowired
    private ConfigurableApplicationContext springContext;
    //springContext.getBeanFactory().destroyScopedBean();

    private String zkServerAddress = null;
    private String zkRootPathWatch = null;
    private String localSavedPath = null;

    private List<BeanValueWatching> watchingBeanList = new ArrayList<>();

    private final Integer CONFIG_ADDED = 1;
    private final Integer CONFIG_UPDATEED = 2;
    private final Integer CONFIG_REMOVED = 3;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        zkServerAddress = zkCenterSetting.getConfigAddress();
        zkRootPathWatch = ZkCenterSetting.ZKCENTER_PATH_PREFIX + "/" + zkCenterSetting.getProjectName() + "/" + zkCenterSetting.getApplicationName() + "/" + zkCenterSetting.getActiveProfile();
        String userHomeDir = FileUtil.getUserHomePath();
        localSavedPath = userHomeDir + (userHomeDir.endsWith(File.separator) ? "" : File.separator) + "localZkCenter" + File.separator + zkCenterSetting.getProjectName() + File.separator;

        ApplicationContext applicationContext = contextRefreshedEvent.getApplicationContext();
        if (applicationContext.getParent() == null) {
            // handle @EnableZkCenter annotation, init zkCenterSetting bean
            boolean enableZkCenterFlag = false;
            Map<String, Object> enableObjectMap = applicationContext.getBeansWithAnnotation(EnableZkCenter.class);
            for (Map.Entry<String, Object> objectEntry : enableObjectMap.entrySet()) {
                EnableZkCenter enableAnnotation = objectEntry.getValue().getClass().getAnnotation(EnableZkCenter.class);
                if (enableAnnotation != null){
                    // 如果发现@EnableZkCenter注解,则启用ZkCenter功能
                    enableZkCenterFlag = true;
                    break;
                }
            }

            // debug for dev the zkcenter injection function
            // load ZkCenter data from local file
            if (enableZkCenterFlag) {
                // 遍历所有bean中的@ZkCenterValue注解,暂存进list
                Arrays.stream(applicationContext.getBeanDefinitionNames()).forEach(beanName->{
                    Object beanObj = applicationContext.getBean(beanName);
                    Arrays.stream(beanObj.getClass().getDeclaredFields()).forEach(field -> {
                        ZkCenterValue annotation = field.getAnnotation(ZkCenterValue.class);
                        if (annotation!=null){
                            String annotationValue = annotation.value();

                            BeanValueWatching watchingBean = new BeanValueWatching();
                            watchingBean.setBeanName(beanName);
                            watchingBean.setObject(beanObj);
                            watchingBean.setFieldName(field.getName());
                            watchingBean.setWatchingValue(annotationValue);
                            watchingBeanList.add(watchingBean);
                        }
                    });
                });

                // 先使用本地的配置进行初始化
                loadLocalConfigData();
                log.debug("EnableZkCenter init: {}", zkCenterSetting);
                System.err.println("EnableZkCenter init: " + zkCenterSetting);

                // start to watch ZkCenter
                initWatchConfigCenter();
            }
        }
    }

    private void initWatchConfigCenter() {
        if (StrUtil.isBlank(zkServerAddress)) {
            log.debug("zkServerAddress is blank, abort watching !");
            return;
        }

        CuratorFramework curatorClient = CuratorFrameworkFactory.newClient(zkServerAddress, new RetryNTimes(3, 1000));
        curatorClient.start();

        try {
            TreeCache treeCache = new TreeCache(curatorClient, zkRootPathWatch);
            treeCache.getListenable().addListener(
                    (client, event) -> {
                        boolean needPublish = false;
                        Integer oper = null;
                        String key = null;
                        String value = null;

                        switch (event.getType()){
                            case NODE_ADDED:
                                oper = CONFIG_ADDED;
                                needPublish = true;
                                break;
                            case NODE_UPDATED:
                                oper = CONFIG_UPDATEED;
                                needPublish = true;
                                break;
                            case NODE_REMOVED:
                                oper = CONFIG_REMOVED;
                                needPublish = true;
                                break;
                            default:
                                log.debug("recv a unknow Event: ", JSONUtil.toJsonStr(event));
                                break;
                        }

                        if (needPublish) {
                            //System.err.println("TreeCache.event: " + event.toString());
                            key = event.getData().getPath();
                            value = new String(event.getData().getData(), CharsetUtil.GBK);

                            if (key.startsWith(zkRootPathWatch)) {
                                key = key.replace(zkRootPathWatch +"/", "");
                                // save to local and publish
                                storeChangingToLocalFile(oper, key, value);
                            }
                        }
                    }
            );
            treeCache.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // load local properties/yml file and publish it's content to init configCenter Data
    private void loadLocalConfigData() {
        if (!FileUtil.exist(localSavedPath) || !FileUtil.isDirectory(localSavedPath))
            return;

        for (String fileName : FileUtil.listFileNames(localSavedPath)) {
            String fullFileName = localSavedPath + fileName;
            log.debug("loading parameters from localSavedFile: {}", fullFileName);
            publishConfigChanging(CONFIG_ADDED, fileName, FileUtil.readString(fullFileName, CharsetUtil.defaultCharset()));
        }
    }

    // save every changing into local properties file
    private void storeChangingToLocalFile(Integer operation, String nodeName, String nodeValue) {
        // System.err.println("storeChangingToLocalFile... " + operation + ": " + nodeName + ": " + nodeValue);
        // 由于去掉了对nodeValue为空则退出的校验,zk的路径变化也会被save,这样就会出现异常,因为localSavedFile="C:\xxx\yyy\/zkcenter/egmall/abc/dev"
        if (StrUtil.isBlank(nodeName) || nodeName.indexOf("/")>=0)
            return;

        long time = new Date().getTime();
        if (!FileUtil.exist(localSavedPath) || !FileUtil.isDirectory(localSavedPath))
            FileUtil.mkdir(localSavedPath);
        String localSavedFile = localSavedPath + nodeName;

        try {
            if (operation == CONFIG_REMOVED) {
                // System.err.println("remove localFile... " + localSavedFile);
                FileUtil.newFile(localSavedFile).deleteOnExit();
            } else {
                // System.err.println("add or update localFile... " + localSavedFile);
                if (!FileUtil.exist(localSavedFile)) {
                    if (!FileUtil.newFile(localSavedFile).createNewFile()){
                        log.debug("create local file failed! {}", localSavedFile);
                        return;
                    }
                }
                if (FileUtil.readUtf8String(localSavedFile).equals(nodeValue)) {
                    log.debug("same as old config data, not need to refresh bean");
                    return;
                } else {
                    FileUtil.writeUtf8String(nodeValue, localSavedFile);
                    log.debug("update local file, cost: {}", (new Date().getTime() - time) + "ms");
                }

                publishConfigChanging(operation, nodeName, nodeValue);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // publish the change from ZkCenter to resourceMap

    /**
     * TODO: 删除zk节点时的处理逻辑尚未添加,思考ing......
     * @param operation
     * @param nodeName
     * @param nodeValue
     */
    private void publishConfigChanging(Integer operation, String nodeName, String nodeValue) {
        if (StrUtil.isBlank(nodeName))
            return;

        log.debug("publishConfigChanging: {}:{}={}", operation, nodeName, nodeValue);
        try {
            List<PropertySource<?>> propertySourcesList = new ArrayList<>();
            /* txt, json, xml, yaml, html, properties */
            if (FileNameUtil.extName(nodeName).equals("yml") ||FileNameUtil.extName(nodeName).equals("yaml")) {
                // 处理yaml配置文件, 封装成 YamlPropertySource
                ByteArrayResource byteArrayResource = new ByteArrayResource(nodeValue.getBytes(StandardCharsets.UTF_8));
                List<PropertySource<?>> ymlSourceList = new YamlPropertySourceLoader().load(nodeName, byteArrayResource);
                // 有可能是多个
                propertySourcesList.addAll(ymlSourceList);
            } else if (FileNameUtil.extName(nodeName).equals("properties")) {
                // 处理properties配置文件, 封装成 PropertiesPropertySource
                Properties properties = new Properties();
                properties.load(new StringReader(nodeValue));
                PropertiesPropertySource propSource = new PropertiesPropertySource(nodeName, properties);
                // 单个
                propertySourcesList.add(propSource);
            } else if (FileNameUtil.extName(nodeName).equals("xml")) {
                // 处理xml配置文件
                watchingBeanList.forEach(item->{
                    if (nodeName.equals(item.getWatchingValue())) {
                        // System.err.println("name: " + item.getBeanName() + ", obj:" + item.getObject() + ", field: " + item.getFieldName() + ", value: " + nodeValue);
                        Object bean = springContext.getBean(item.getBeanName());
                        BeanUtil.setFieldValue(bean, item.getFieldName(), nodeValue);
                    }
                });
            } else if (FileNameUtil.extName(nodeName).equals("txt")) {
                // 处理txt配置文件
                System.err.println("publishConfigChanging ... txt: " + nodeName + " -- " + nodeValue);
            } else if (FileNameUtil.extName(nodeName).equals("html")) {
                // 处理html配置文件
                System.err.println("publishConfigChanging ... html: " + nodeName + " -- " + nodeValue);
            } else {
                System.err.println("unknown config content: " + nodeName + "\n" + nodeValue);
            }

            if (propertySourcesList.size()>0){
                // 将新接收到的一个或多个propertySource 注入到spring的PropertySources中
                MutablePropertySources propertySources = springContext.getEnvironment().getPropertySources();
                for (PropertySource<?> propertySource : propertySourcesList) {
                    if (propertySources.get(propertySource.getName()) == null) {
                        propertySources.addLast(propertySource);
                    } else {
                        propertySources.replace(propertySource.getName(), propertySource);
                    }
                }

                // 刷新相关的bean
                // System.err.println("refreshScopedBean ...");
                refreshScopedBean();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // after publish properties, refresh beans with our own annotation "ZkCenterScope"
    private void refreshScopedBean() {
        // springContext.refresh(); // 另一种用法: https://blog.csdn.net/qq_27467439/article/details/108454807
        Map<String, Object> ScopedBeansMap = springContext.getBeansWithAnnotation(ZkCenterScope.class);
        ScopedBeansMap.forEach((beanName, beanObj)->springContext.getBeanFactory().destroyScopedBean(beanName));
    }

}

class BeanValueWatching {
    private String beanName;
    private Object object;
    private String fieldName;
    private String watchingValue;

    public String getBeanName() {
        return beanName;
    }
    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }
    public Object getObject() {
        return object;
    }
    public void setObject(Object object) {
        this.object = object;
    }
    public String getFieldName() {
        return fieldName;
    }
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
    public String getWatchingValue() {
        return watchingValue;
    }
    public void setWatchingValue(String watchingValue) {
        this.watchingValue = watchingValue;
    }
    @Override
    public String toString() {
        return "BeanValueWatching{" +
                "beanName='" + beanName + '\'' +
                ", fieldName='" + fieldName + '\'' +
                ", watchingValue='" + watchingValue + '\'' +
                ", object=" + object +
                '}';
    }
}